"""add column quality

Revision ID: 0965670a3d41
Revises: b1817126ff00
Create Date: 2024-04-19 19:59:24.096510

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '0965670a3d41'
down_revision: Union[str, None] = 'b1817126ff00'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('visualization', sa.Column('quality', sa.String(), nullable=True))
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('visualization', 'quality')
    # ### end Alembic commands ###
