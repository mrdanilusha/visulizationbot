"""add table optimization_method_diagram

Revision ID: 5d5b6d7c10b3
Revises: 4cc73762673d
Create Date: 2024-04-26 21:44:17.704707

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '5d5b6d7c10b3'
down_revision: Union[str, None] = '4cc73762673d'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('optimization_method_diagram',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('path', sa.String(), nullable=True),
    sa.Column('load_link', sa.String(), nullable=True),
    sa.Column('file_id', sa.String(), nullable=True),
    sa.Column('format', sa.String(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('optimization_method_diagram')
    # ### end Alembic commands ###
