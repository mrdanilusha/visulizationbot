from typing import BinaryIO

from celery import Celery
import subprocess

app = Celery('tasks', broker='amqp://root:root@localhost//')


@app.task
def render_visualization_task(method_name: str, description: str, file_data: bytes):
    with open("test_manim.py", "w") as file:
        file.write(file_data.decode())
    try:
        test = subprocess.Popen(["manim", "-qh", "test_manim.py"], stdout=subprocess.PIPE)
    except Exception as exc:
        print("Errror!")

    process_pid = test.pid
    output = test.communicate()[0]
    print(output)

