import asyncio
import html
import json
import logging
import random
import sys
import types

import aiogram.types
import sqlalchemy
from aiogram import Dispatcher, Bot, Router, F
from aiogram.filters import CommandStart, Command
from aiogram.fsm.context import FSMContext
from aiogram.fsm.state import StatesGroup, State
from aiogram.fsm.storage.memory import MemoryStorage
from aiogram.types import BotCommand, Message, InlineKeyboardButton, InputFile, BufferedInputFile, FSInputFile, \
    Document, Video
from aiogram.types.callback_query import CallbackQuery
from aiogram.utils.keyboard import InlineKeyboardBuilder
from sqlalchemy import select, insert, delete, cast
from sqlalchemy.exc import IntegrityError

from config import dp, StateMachine, DIVISION_NUMBER, bot
from handlers import load_diagram, load_visualization
from db import async_session
from models import Visualization, OptimizationMethod, OptimizationMethodDiagram
from utils import message_preparation, message_preparation_diagram, message_delete_method


@dp.message(CommandStart())
async def command_start_handler(message: Document) -> None:
    """
    This handler receives messages with `/start` command
    """
    await message.answer(
        "Добро пожаловать! Доступны следующие команды:\n1. /load_visualization - загрузка визуализации\n"
        "2./load_diagram - загрузка блок-схемы"
    )

@dp.message(Command("add_method"))
@dp.message(Command("delete_method"))
@dp.message(Command("edit_method_group"))
@dp.message(Command("edit_method_name"))
@dp.message(Command("add_visualization"))
@dp.message(Command("delete_visualization"))
@dp.message(Command("edit_visualization_description"))
@dp.message(Command("edit_visualization_resolution"))
@dp.message(Command("edit_visualization_file"))
@dp.message(Command("edit_visualization_method"))
@dp.message(Command("add_diagram"))
@dp.message(Command("delete_diagram"))
@dp.message(Command("edit_diagram_method"))
@dp.message(Command("edit_diagram_file"))
async def commands(message: Message, state: FSMContext) -> None:
    await message.answer("У Вас недостаточно прав для выполнения команды")


@dp.message(Command("add_visualization"))
async def add_visualization(message: Message, state: FSMContext) -> None:
    await state.set_state(StateMachine.add_visualization)

    builder = InlineKeyboardBuilder()
    async with async_session() as session:
        stmt = (
            select(OptimizationMethod)
            )

        result = (await session.execute(stmt)).scalars().all()

        for method in result:
            builder.row(
                InlineKeyboardButton(
                    text=method.name,
                    callback_data=str(method.id),
                    )
                ),

    await message.answer("Выберите численный метод", reply_markup=builder.as_markup())
    await state.set_state(StateMachine.add_visualization_four)


@dp.callback_query(F.data.regexp("^[480|720|1080|2160]"), StateMachine.add_visualization_second)
async def add_video(callback: CallbackQuery, state: FSMContext) -> None:
    await state.set_state(StateMachine.add_visualization_second)
    await state.update_data(
        resolution=callback.data
    )
    await callback.message.answer("Введите описание к визуализации")


@dp.message(StateMachine.add_visualization_second)
async def send_document(message: Message, state: FSMContext) -> None:
    await state.update_data(
        description=message.text
    )
    await message.answer("Отправьте видео как документ")
    await state.set_state(StateMachine.add_visualization_third)


@dp.callback_query(StateMachine.add_visualization_four)
async def choose_resolution(callback: CallbackQuery, state: FSMContext) -> None:
    await state.update_data(
        optimization_method_id=callback.data
    )
    builder = InlineKeyboardBuilder()

    builder.row(
        *[
            InlineKeyboardButton(
                text="480",
                callback_data="480"
            ),
            InlineKeyboardButton(
                text="720",
                callback_data="720"
            ),
            InlineKeyboardButton(
                text="1080",
                callback_data="1080"
            ),
            InlineKeyboardButton(
                text="2160",
                callback_data="2160"
            )
        ]
    )

    await callback.message.answer("Выберите разрешение видео", reply_markup=builder.as_markup())
    await state.set_state(StateMachine.add_visualization_second)


@dp.message(StateMachine.add_visualization_third)
async def send_document(message: Document, state: FSMContext) -> None:
    data = await state.get_data()
    print(message)
    async with async_session() as session:
        stmt = (
            insert(Visualization).values(
                {
                    "description": data.get("description"),
                    "file_id": message.document.file_id,
                    "quality": data.get("resolution"),
                    "load_link": "/load" + str(random.randint(1, 10**3)),
                    "optimization_method_id": int(data.get("optimization_method_id"))
                }
            )
        )
        await session.execute(stmt)
        await session.commit()


@dp.message(Command("add_diagram"))
async def add_diagram(message: Message, state: FSMContext) -> None:
    await state.set_state(StateMachine.add_diagram_first)

    builder = InlineKeyboardBuilder()
    async with async_session() as session:
        stmt = (
            select(OptimizationMethod)
            )

        result = (await session.execute(stmt)).scalars().all()

        for method in result:
            builder.row(
                InlineKeyboardButton(
                    text=method.name,
                    callback_data=str(method.id),
                    )
                ),

    await message.answer("Выберите численный метод", reply_markup=builder.as_markup())
    await state.set_state(StateMachine.add_diagram_second)


@dp.callback_query(StateMachine.add_diagram_second)
async def send_document(callback: CallbackQuery, state: FSMContext) -> None:
    await state.update_data(
        optimization_method_id=callback.data
    )
    await callback.message.answer("Отправьте блок-схему как документ")
    await state.set_state(StateMachine.add_diagram_third)


@dp.message(StateMachine.add_diagram_third)
async def insert_diagram(message: Document, state: FSMContext):
    data = await state.get_data()
    print(message)
    async with async_session() as session:
        stmt = (
            select(OptimizationMethodDiagram).where(
                OptimizationMethodDiagram.optimization_method_id == int(data.get("optimization_method_id"))
            ).distinct(
                OptimizationMethodDiagram.format
            )
        )

        dat = (await session.scalars(stmt)).all()
    async with async_session() as session:
        stmt = (
            insert(OptimizationMethodDiagram).values(
                {
                    "file_id": message.document.file_id,
                    "format": message.document.file_name.split('.')[-1],
                    "load_link": dat[-1].load_link if dat else "/load" + str(random.randint(1, 10 ** 3)),
                    "optimization_method_id": int(data.get("optimization_method_id"))
                }
            )
        )
        await session.execute(stmt)
        await session.commit()


@dp.message(Command("add_method"))
async def add_new_method(message: Message, state: FSMContext):
    await state.set_state(StateMachine.add_method_group_first)
    builder = InlineKeyboardBuilder()
    builder.row(
        *[
            InlineKeyboardButton(
                text='Одномерные',
                callback_data='single'
            ),
            InlineKeyboardButton(
                text='Многомерные',
                callback_data='multiple'
            )
        ]
    )
    await message.answer("Выберите категорию численных методов", reply_markup=builder.as_markup())


@dp.callback_query(StateMachine.add_method_group_first)
async def add_new_method(callback: CallbackQuery, state: FSMContext):
    await state.set_data({"method_group": callback.data})
    await callback.message.answer("Введите название нового метода")
    await state.set_state(StateMachine.add_method_group_second)


@dp.message(Command("cancel"))
async def cancel(message: Message, state: FSMContext):
    await state.clear()
    await message.answer(
        "Доступны следующие команды:\n1. /load_visualization - загрузка визуализации\n"
        "2./load_diagram - загрузка блок-схемы"
    )


@dp.message(StateMachine.add_method_group_second)
async def add_new_method(message: Message, state: FSMContext):
    data = await state.get_data()
    try:
        async with async_session() as session:
            stmt = (
                insert(OptimizationMethod).values(
                    {
                        "name": message.text,
                        "group": data["method_group"],
                        "link": "/method" + str(random.randint(1, 10**6)),

                    }
                )
            )

            await session.execute(stmt)
            await session.commit()
            await message.answer("Метод успешно добавлен")
    except IntegrityError:
        await message.answer("Данный метод уже существует. Введите новое название или нажмите /cancel")


@dp.message(Command("delete_method"))
async def delete_method(message: Message, state: FSMContext):
    builder = InlineKeyboardBuilder()
    builder.row(
        *[
            InlineKeyboardButton(
                text='Одномерные',
                callback_data='single'
            ),
            InlineKeyboardButton(
                text='Многомерные',
                callback_data='multiple'
            )
        ]
    )
    await message.answer("Выберите категорию численных методов", reply_markup=builder.as_markup())
    await state.set_state(StateMachine.delete_method_group_first)


@dp.message(F.text.contains('method'), StateMachine.delete_method_group_first)
async def delete_algorithm(message: Message, state: FSMContext):
    stmt = (
        delete(
            OptimizationMethod
        ).where(
            OptimizationMethod.link == message.text
        )
    )

    async with async_session() as session:
        await session.execute(stmt)
        await session.commit()

    await message.answer("Метод успешно удален")
    await state.clear()


@dp.callback_query(F.data.regexp("^[0-9]"), StateMachine.delete_method_group_first)
async def call_back_change(callback: CallbackQuery, state: FSMContext):
    data = await state.get_data()

    async with async_session() as session:
        stmt = (
            select(OptimizationMethod).where(
                OptimizationMethod.group == data["group"]
            )
        )

        result = (await session.execute(stmt)).scalars().all()

        pages = round(len(result) / DIVISION_NUMBER)

        builder = InlineKeyboardBuilder()
        list_buttons = [
            InlineKeyboardButton(
                text=str(page + 1),
                callback_data=str(page + 1)
            ) for page in range(pages)
        ]

        builder.row(
            *list_buttons
        )

        prepared_message = await message_delete_method(result[(int(callback.data) - 1) * 2: int(callback.data) * 2])

        await bot.edit_message_text(
            text='\n\n'.join(prepared_message),
            chat_id=callback.message.chat.id,
            message_id=data["message_edit_id"],
            parse_mode='MarkdownV2',
            reply_markup=builder.as_markup(),
        )


@dp.callback_query((F.data == "single") | (F.data == "multiple"), StateMachine.delete_method_group_first)
async def delete_method(callback: CallbackQuery, state: FSMContext):
    async with async_session() as session:
        stmt = (
            select(OptimizationMethod)
            .where(
                OptimizationMethod.group == callback.data
            )
        )
        result = (await session.execute(stmt)).scalars().all()
        pages = round(len(result) / DIVISION_NUMBER)

        builder = InlineKeyboardBuilder()
        list_buttons = [
            InlineKeyboardButton(
                text=str(page + 1),
                callback_data=str(page + 1)
            ) for page in range(pages)
        ]

        builder.row(
            *list_buttons
        )

        prepared_message = await message_delete_method(result[0:DIVISION_NUMBER])

        message = await bot.send_message(
            chat_id=callback.message.chat.id,
            text='\n\n'.join(prepared_message),
            parse_mode='MarkdownV2',
            reply_markup=builder.as_markup()
        )

        await state.set_data(
            {
                "message_edit_id": message.message_id,
                "group": callback.data,
            }
        )


async def main() -> None:
    load_visualization_command = BotCommand(
        command="load_visualization",
        description="Cкачивание визуализаций"
    )
    load_diagram_command = BotCommand(
        command="load_diagram",
        description="Скачивание блок-схемы"
    )

    await bot.set_my_commands(
        commands=[
            load_visualization_command,
            load_diagram_command,
        ]
    )

    dp.include_routers(load_diagram.router, load_visualization.router)

    await dp.start_polling(bot)

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, stream=sys.stdout)
    asyncio.run(main())
