import json

import sqlalchemy
from aiogram import Router, F
from aiogram.filters import Command
from aiogram.fsm.context import FSMContext
from aiogram.types import Message, InlineKeyboardButton, CallbackQuery, FSInputFile
from aiogram.utils.keyboard import InlineKeyboardBuilder
from sqlalchemy import select, cast
from db import async_session

from config import StateMachine, DIVISION_NUMBER, bot
from models import OptimizationMethod, Visualization
from utils import message_preparation, prepare_keyboard

router = Router()


@router.message(Command("load_visualization"))
async def load_visualization(message: Message, state: FSMContext) -> None:
    await state.set_state(StateMachine.load_visualization)
    builder = InlineKeyboardBuilder()
    builder.row(
        *[
            InlineKeyboardButton(
                text='Одномерные',
                callback_data='single'
            ),
            InlineKeyboardButton(
                text='Многомерные',
                callback_data='multiple'
            )
        ]
    )
    await message.answer("Выберите категорию численных методов", reply_markup=builder.as_markup())


@router.callback_query(F.data.regexp("^[0-9]"), StateMachine.load_visualization)
async def call_back_change(callback: CallbackQuery, state: FSMContext):
    data = await state.get_data()
    async with async_session() as session:
        stmt = (
            select(Visualization)
            .join(OptimizationMethod)
            .where(
                OptimizationMethod.group == data["group"]
            ).distinct(
                Visualization.load_link
            )
        )
        result = (await session.execute(stmt)).scalars().all()

    pages = round(len(result) // DIVISION_NUMBER)

    pages, builder = await prepare_keyboard(result=result, pages=pages)

    prepared_message = await message_preparation(result[(int(callback.data) - 1) * 2: int(callback.data) * 2])

    await bot.edit_message_text(
        text='\n\n'.join(prepared_message),
        chat_id=callback.message.chat.id,
        message_id=data["message_edit_id"],
        parse_mode='MarkdownV2',
        reply_markup=builder.as_markup(),
    )


@router.callback_query((F.data == "single") | (F.data == "multiple"), StateMachine.load_visualization)
async def single_callback(callback: CallbackQuery, state: FSMContext):
    async with async_session() as session:
        stmt = (
            select(Visualization)
            .join(OptimizationMethod)
            .where(
                OptimizationMethod.group == callback.data
            )
        ).distinct(Visualization.load_link)
        result = (await session.execute(stmt)).scalars().all()
        pages = round(len(result) // DIVISION_NUMBER)

        pages, builder = await prepare_keyboard(result=result, pages=pages)

        prepared_message = await message_preparation(result[0:DIVISION_NUMBER])

        message = await bot.send_message(
            chat_id=callback.message.chat.id,
            text='\n\n'.join(prepared_message),
            parse_mode='MarkdownV2',
            reply_markup=builder.as_markup()
        )

        await state.set_data(
            {
                "message_edit_id": message.message_id,
                "visualizations": result,
                "group": callback.data,
            }
        )


@router.callback_query(StateMachine.load_visualization)
async def call_back_send_video(callback: CallbackQuery, state: FSMContext):
    callback_data = json.loads(callback.data)
    async with async_session() as session:
        stmt = (
            select(Visualization)
            .where(
                Visualization.load_link == callback_data.get("video"),
                Visualization.quality == callback_data.get("quality")
            )

        )
        result = await session.scalar(stmt)

        if result.file_id:
            await bot.send_document(
                chat_id=callback.message.chat.id,
                document=result.file_id,
            )
        else:
            data = await bot.send_document(
                chat_id=callback.message.chat.id,
                document=FSInputFile(
                    path=result.path,
                    filename=result.optimization_method.name + '.mp4',
                )
            )

        if not result.file_id:
            result.file_id = data.document.file_id

            await session.commit()


@router.message(F.text.contains('load_v'), StateMachine.load_visualization)
async def suggest_video_quality(message: Message):
    builder = InlineKeyboardBuilder()
    async with async_session() as session:
        stmt = (
            select(Visualization)
            .where(
                Visualization.load_link.contains(message.text.split('_')[-1]),
            ).order_by(
                cast(Visualization.quality, sqlalchemy.Integer)
            )

        )
        result = (await session.scalars(stmt)).all()
        buttons = []
        for visualization in result:
            buttons.append(
                InlineKeyboardButton(
                    text=visualization.quality,
                    callback_data=json.dumps(
                        {
                            "quality": visualization.quality,
                            "video": visualization.load_link,
                        }
                    )
                ),
            )

    builder.row(*buttons)

    await message.answer("Выберите качество видео", reply_markup=builder.as_markup())
