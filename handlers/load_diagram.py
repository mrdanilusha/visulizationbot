import json

from aiogram import Router, F
from aiogram.filters import Command
from aiogram.fsm.context import FSMContext
from aiogram.fsm.storage.base import StorageKey
from aiogram.types import Message, InlineKeyboardButton, CallbackQuery, FSInputFile
from aiogram.utils.keyboard import InlineKeyboardBuilder
from sqlalchemy import select
from db import async_session

from config import StateMachine, storage, DIVISION_NUMBER, bot
from models import OptimizationMethodDiagram, OptimizationMethod
from utils import message_preparation_diagram

router = Router()


@router.callback_query(F.data.regexp("^[0-9]"), StateMachine.load_diagram)
async def call_back_change_load_diagram(callback: CallbackQuery):
    data = await storage.get_data(
        key=StorageKey(
            bot_id=bot.id,
            chat_id=callback.message.chat.id,
            user_id=callback.message.from_user.id,
        )
    )

    async with async_session() as session:
        stmt = (
            select(OptimizationMethodDiagram)
            .join(OptimizationMethod)
            .where(
                OptimizationMethod.group == data["group"]
            )
        ).distinct(
            OptimizationMethodDiagram.load_link
        )
        result = (await session.execute(stmt)).scalars().all()

    pages = round(len(result) // DIVISION_NUMBER)

    if len(result) % 2 != 0:
        pages = pages + 1

    builder = InlineKeyboardBuilder()
    list_buttons = [
        InlineKeyboardButton(
            text=str(page + 1),
            callback_data=str(page + 1)
        ) for page in range(pages)
    ]

    builder.row(
        *list_buttons
    )

    prepared_message = await message_preparation_diagram(result[(int(callback.data) - 1) * 2: int(callback.data) * 2])

    await bot.edit_message_text(
        text='\n\n'.join(prepared_message),
        chat_id=callback.message.chat.id,
        message_id=data["message_edit_id"],
        parse_mode='MarkdownV2',
        reply_markup=builder.as_markup(),
    )


@router.callback_query((F.data == "single") | (F.data == "multiple"), StateMachine.load_diagram)
async def single_callback_load_diagram(callback: CallbackQuery, state: FSMContext):
    async with async_session() as session:
        stmt = (
            select(OptimizationMethodDiagram)
            .join(OptimizationMethod)
            .where(
                OptimizationMethod.group == callback.data
            )
        ).distinct(OptimizationMethodDiagram.load_link)
        result = (await session.execute(stmt)).scalars().all()
        pages = round(len(result) // DIVISION_NUMBER)

        if len(result) % 2 != 0:
            pages = pages + 1

        builder = InlineKeyboardBuilder()
        list_buttons = [
            InlineKeyboardButton(
                text=str(page + 1),
                callback_data=str(page + 1)
            ) for page in range(pages)
        ]

        builder.row(
            *list_buttons
        )

        prepared_message = await message_preparation_diagram(result[0:DIVISION_NUMBER])
        message = await bot.send_message(
            chat_id=callback.message.chat.id,
            text='\n\n'.join(prepared_message),
            parse_mode='MarkdownV2',
            reply_markup=builder.as_markup()
        )

        await storage.set_data(
            key=StorageKey(
                bot_id=bot.id,
                chat_id=message.chat.id,
                user_id=message.from_user.id,
            ),
            data={
                "message_edit_id": message.message_id,
                "diagram": result,
                "group": callback.data,
            }
        )


@router.callback_query(StateMachine.load_diagram)
async def call_back_send_diagram(callback: CallbackQuery, state: FSMContext):
    callback_data = json.loads(callback.data)
    async with async_session() as session:
        stmt = (
            select(OptimizationMethodDiagram)
            .where(
                OptimizationMethodDiagram.load_link == callback_data.get("diagram"),
                OptimizationMethodDiagram.format == callback_data.get("quality")
            )

        )
        result = await session.scalar(stmt)

        if result.file_id:
            await bot.send_document(
                chat_id=callback.message.chat.id,
                document=result.file_id,
            )
        else:
            data = await bot.send_document(
                chat_id=callback.message.chat.id,
                document=FSInputFile(
                    path=result.path,
                    filename="test" + "." + result.format
                )
            )

        if not result.file_id:
            result.file_id = data.document.file_id

            await session.commit()


@router.message(F.text.regexp(r'^/load_d[0-9]'), StateMachine.load_diagram)
async def suggest_video_quality(message: Message):
    async with async_session() as session:
        stmt = (
            select(OptimizationMethodDiagram)
            .where(
                OptimizationMethodDiagram.load_link.contains(message.text.split('_')[-1]),
            )

        )
        result = (await session.scalars(stmt)).all()
        builder = InlineKeyboardBuilder()
        buttons = []
        for diagram in result:
            buttons.append(
                InlineKeyboardButton(
                    text=diagram.format,
                    callback_data=json.dumps(
                        {
                            "quality": diagram.format,
                            "diagram": diagram.load_link
                        }
                    )
                )
            )
        builder.row(*buttons)

    await message.answer("Выберите формат блок-схемы", reply_markup=builder.as_markup())


@router.message(Command("load_diagram"))
async def test(message: Message, state: FSMContext):
    await state.set_state(StateMachine.load_diagram)
    builder = InlineKeyboardBuilder()
    builder.row(
        *[
            InlineKeyboardButton(
                text='Одномерные',
                callback_data='single'
            ),
            InlineKeyboardButton(
                text='Многомерные',
                callback_data='multiple'
            )
        ]
    )

    await message.answer("Выберите категорию численных методов", reply_markup=builder.as_markup())
