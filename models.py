from typing import List

import sa as sa
from sqlalchemy import ForeignKey, UniqueConstraint
from sqlalchemy.orm import DeclarativeBase, mapped_column, Mapped, relationship


class Base(DeclarativeBase):
    pass


class Visualization(Base):
    __tablename__ = "visualization"

    id: Mapped[int] = mapped_column(primary_key=True)
    description: Mapped[str] = mapped_column(nullable=True)
    optimization_method_id: Mapped[int] = mapped_column(
        ForeignKey("optimization_method.id"),
        nullable=True,
    )
    quality: Mapped[str] = mapped_column(nullable=True)
    load_link: Mapped[str] = mapped_column(nullable=True)
    file_id: Mapped[str] = mapped_column(nullable=True)

    optimization_method: Mapped["OptimizationMethod"] = relationship(
        back_populates="visulizations",
        lazy='selectin'
    )


class OptimizationMethodDiagram(Base):
    __tablename__ = "optimization_method_diagram"

    id: Mapped[int] = mapped_column(primary_key=True)
    load_link: Mapped[str] = mapped_column(nullable=True)
    file_id: Mapped[str] = mapped_column(nullable=True)
    format: Mapped[str] = mapped_column(nullable=True)
    optimization_method_id: Mapped[int] = mapped_column(
        ForeignKey("optimization_method.id"),
        nullable=True,
    )

    optimization_method: Mapped["OptimizationMethod"] = relationship(
        back_populates="diagrams",
        lazy='selectin'
    )


class OptimizationMethod(Base):
    __tablename__ = "optimization_method"
    __table_args__ = (UniqueConstraint("name", "group", name="unq_name_group"),)

    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(nullable=True)
    group: Mapped[str] = mapped_column(nullable=True)
    link: Mapped[str] = mapped_column(nullable=True)

    visulizations: Mapped[list[Visualization]] = relationship(
        back_populates="optimization_method",
        lazy='selectin'
    )

    diagrams: Mapped[list[OptimizationMethodDiagram]] = relationship(
        back_populates="optimization_method",
        lazy='selectin'
    )
