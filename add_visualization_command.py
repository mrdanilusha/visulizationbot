import json

from aiogram.filters import Command
from aiogram.fsm.context import FSMContext
from aiogram.types import Message, InlineKeyboardButton
from aiogram.utils.keyboard import InlineKeyboardBuilder
from sqlalchemy import select

from db import async_session
from main import StateMachine, dp
from models import OptimizationMethod


@dp.message(Command("add_visualization"))
async def add_visualization(message: Message, state: FSMContext) -> None:
    await state.set_state(StateMachine.add_visualization)

    builder = InlineKeyboardBuilder()
    async with async_session() as session:
        stmt = (
            select(OptimizationMethod)
            )

        result = (await session.execute(stmt)).scalars().all()

        buttons = []
        for method in result:
            buttons.append(
                InlineKeyboardButton(
                    text=method.name,
                    callback_data=json.dumps(
                        {
                            "name": method.name,
                        }
                    )
                ),
            )

    builder.row(*buttons)

    await message.answer("Выберите численный метод", reply_markup=builder.as_markup())

