from typing import List

from aiogram.types import InlineKeyboardButton
from aiogram.utils.keyboard import InlineKeyboardBuilder

from models import Visualization, OptimizationMethodDiagram, OptimizationMethod


async def message_preparation(visualizations: List[Visualization]):
    text = []
    for index, value in enumerate(visualizations):
        temp_str = '📚' + ' *' + value.optimization_method.name + "*" + '\n' + '__Описание__:\n' \
               + value.description.replace('\\n', '\n') + '\n' + '__Чтобы скачать визуализацию нажмите__: \n' + value.load_link
        temp = temp_str.replace('.', '\.')
        text.append(temp)

    return text


async def message_preparation_diagram(diagrams: List[OptimizationMethodDiagram]):
    text = []
    for index, value in enumerate(diagrams):
        text.append(
            '📚' + ' *' + value.optimization_method.name + "*"
            + '\n __Чтобы скачать блок\-схему нажмите__: \n' + value.load_link
        )

    return text


async def message_delete_method(methods: List[OptimizationMethod]):
    text = []
    for index, value in enumerate(methods):
        text.append(
            "*" + str(index + 1) + '\. ' + value.name + "*"
            + '\n__Ссылка на удаление метода__: \n' + value.link
        )

    return text


async def prepare_keyboard(result, pages):
    builder = InlineKeyboardBuilder()

    if len(result) % 2 != 0:
        pages = pages + 1

    list_buttons = [
        InlineKeyboardButton(
            text=str(page + 1),
            callback_data=str(page + 1)
        ) for page in range(pages)
    ]

    builder.row(
        *list_buttons
    )

    return pages, builder
