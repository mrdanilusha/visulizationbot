from aiogram import Dispatcher, Bot
from aiogram.fsm.state import StatesGroup, State
from aiogram.fsm.storage.memory import MemoryStorage

TOKEN = "7108550289:AAHrLNfEovGq4FZmUHy2gprcRlT_z-woXi0"

storage = MemoryStorage()

dp = Dispatcher(storage=storage)
bot = Bot(token=TOKEN)

DIVISION_NUMBER = 2


class StateMachine(StatesGroup):
    load_visualization = State()
    load_diagram = State()

    add_visualization = State()
    add_visualization_temp = State()
    add_visualization_second = State()
    add_visualization_third = State()
    add_visualization_four = State()

    add_diagram_first = State()
    add_diagram_second = State()
    add_diagram_third = State()

    add_method_group_first = State()
    add_method_group_second = State()

    delete_method_group_first = State()
